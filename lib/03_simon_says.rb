#Simon says

def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, fre = 2)
  string = ''
  fre.times { string << phrase + ' '}
  string.chop
end

def start_of_word(phrase, where_to_split)
  phrase[0..(where_to_split-1)]
end

def first_word(phrase)
  phrase.split(" ")[0]
end

def titleize(phrase)
  little_words = ["and", "over", "the"]
  first = phrase.split(" ")[0]
  ans = first[0].upcase + first[1..-1] + ' '
  (phrase.split(' ').count - 1).times do |i|
    if little_words.include?(phrase.split(' ')[i+1])
      ans << phrase.split(' ')[i+1] + ' '
    else
      ans << phrase.split(' ')[i+1][0].upcase + phrase.split(' ')[i+1][1..-1] + ' '
    end
  end
  ans.chop
end
