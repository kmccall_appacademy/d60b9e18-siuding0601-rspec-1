# # Calculator
#
# you will build a simple calculator script with the following methods:
#
# `add` takes two parameters and adds them
#
# `subtract` takes two parameters and subtracts the second from the first
#
# `sum` takes an *array* of parameters and adds them all together

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arrayofnums)
  ans = 0
  arrayofnums.each {|n| ans+=n}
  ans
end

# # Bonus
#
# There are also some bonus exercises for when you finish the regular
# ones. The bonus round will help teach you test-driven *development*,
# not simply test-guided *learning*.
#
# Your mission, should you choose to accept it, is to write *tests*
# for three new methods:
#
# * `multiply` which multiplies two numbers together
# * `power` which raises one number to the power of another number
# * `[factorial](http://en.wikipedia.org/wiki/Factorial)` (check
#   Wikipedia if you forgot your high school math).

def multiply(arrayofnums)
  ans = 1
  arrayofnums.each {|n| ans*=n}
  ans
end

def power(num, power)
  num**power
end

def factorial(num)
  ans = 1
  num.downto(1) {|n| ans*=n}
  ans
end
