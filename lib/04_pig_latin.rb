#Piglatin

def translate(phrase)
  vowels = 'aeiou'
  break_phrase = phrase.split(' ')
  ans = ''
  break_phrase.each do |word|
    if vowels.include?(word[0])
      ans << vowel_at_first(word) + ' '
    elsif word.include?("qu")
      ans << qu_finder(word) + ' '
    else
      ans << cos(word) + ' '
    end
  end
  ans.chop
end

def qu_finder(word)
  q_pos = word.split("").find_index("q")
  u_pos = word.split("").find_index("u")
  if u_pos - q_pos == 1
    word[(u_pos + 1)..-1] + word[0..u_pos] + "ay"
  end
end

def vowel_at_first(word)
  word + "ay"
end

def cos(word)
  cos = ''
  vowels = 'aeiou'
  word.split("").each do |al|
    if vowels.include?(al)
      break
    else
      cos << al
    end
  end
  word[cos.length..-1] + cos + "ay"
end
