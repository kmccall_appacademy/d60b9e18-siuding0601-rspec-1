#Temperature conversion function from F <-> C (method:ftoc/ctof)

def ftoc(tempF)
  (tempF - 32.0)*5.0/9.0
end

def ctof(tempC)
  (tempC*9.0/5.0)+32.0
end
